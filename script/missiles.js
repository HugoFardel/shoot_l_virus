//genere un missile provenant du joueur et l'ajoute à la scene et au tableau des missiles
function genereMissile(){
    var geometry = new THREE.BoxGeometry(10, 10, 0);
    var material = new THREE.MeshBasicMaterial({ map: texturePillule, transparent: true });
    ms = new THREE.Mesh(geometry, material);
    ms.position.y = player.position.y+25;
    ms.position.x = player.position.x;
    scene.add(ms);
    missiles.push(ms);
}

//genere un missile ennemie et l'ajoute à la scene et au tableau des missiles ennemies
function genereMissileEnnemie(){
    if(ennemies.length > 0){
        var geometry = new THREE.BoxGeometry(10, 10, 0);
        var material = new THREE.MeshBasicMaterial({ map: textureMissileVirus, transparent: true });
        ms = new THREE.Mesh(geometry, material);
        let random = Math.floor(Math.random()* Math.floor(ennemies.length));
        ms.position.y = ennemies[random].position.y-15;
        ms.position.x = ennemies[random].position.x;
        scene.add(ms);
        missilesEnnemies.push(ms);
    }
}


//déplace tous les missiles (alliés ou ennemies)
//et verifie si il y a collision avec un missile et un joueur ou ennemie
function animateMissile(){
    let distanceX=0, distanceY=0;

    for(let j=0; j<missiles.length; j++){
        missiles[j].position.y+=2;
        for(let i=0; i<ennemies.length; i++){
            distanceX = Math.abs(ennemies[i].position.x - missiles[j].position.x);
            distanceY = Math.abs(ennemies[i].position.y - missiles[j].position.y);

            if(distanceX <= 20 && distanceY <= 20){ //40 car distance max(du centre) player 25 et 15 pour ennemies 
                //scene.remove(ennemies[i]);
                morts.push(ennemies[i]);
                ennemies.splice(i,1);
                document.querySelector("#points").innerHTML = ++points;
                scene.remove(missiles[j]);
                missiles.splice(j,1);
                break;
            }
        }
    };

    //missiles ennemies
    for(let j=0; j<missilesEnnemies.length; j++){
        missilesEnnemies[j].position.y-=speedEnnemie+0.5;
        distanceX = Math.abs(player.position.x - missilesEnnemies[j].position.x);
        distanceY = Math.abs(player.position.y - missilesEnnemies[j].position.y);

        if(distanceX <= 20 && distanceY <= 20){ //40 car distance max(du centre) player 25 et 15 pour ennemies 
            scene.remove(missilesEnnemies[j]);
            missilesEnnemies.splice(j,1);
            document.querySelector("#vie").innerHTML = --hp;
            removeCoeur();
        }
    };

    for(let i=0; i<missiles.length; i++){
        if(missiles[i].position.y > 800){
            scene.remove(missiles[i]);
            missiles.splice(i,1);
        }
    }

    for(let j=0; j<missilesEnnemies.length; j++){
        if(missilesEnnemies[j].position.y < -800){
            scene.remove(missilesEnnemies[j]);
            missilesEnnemies.splice(j,1);
            
        }
    }
}


//fonction générant le laser du joueur
function genereLaser(){
    let hauteur = 400;
    var geometry = new THREE.BoxGeometry(10, hauteur, 0);
    var material = new THREE.MeshBasicMaterial({ map: textureLaser, transparent: true });
    laser = new THREE.Mesh(geometry, material);
    laser.position.y = player.position.y+25+hauteur/2;
    laser.position.x = player.position.x;
    scene.add(laser);
}

//fonction verifiant si il ya contact entre le laser du joueur et un ennemie
function laserTouche(){
    let distanceX=0, distanceY=0;

    if(laser != null){
        for(let i=0; i<ennemies.length; i++){
            distanceX = Math.abs(ennemies[i].position.x - laser.position.x);
            distanceY = Math.abs(ennemies[i].position.y - laser.position.y);

            if(distanceX <= 20 && distanceY <= 215){
                //scene.remove(ennemies[i]);
                morts.push(ennemies[i]);
                ennemies.splice(i,1);
                document.querySelector("#points").innerHTML = ++points;    
            }
        }
    }
}


//animation mort avec opacity 
function animationMorts(){
    for(let i=0; i<morts.length; i++){
        morts[i].material.opacity-=0.05;
        if(morts[i].material.opacity <= 0){
            scene.remove(morts[i]);
        }
    }
}
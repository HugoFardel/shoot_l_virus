//adding all HTML:
// * score
// * vie
// * recommencer
// * aide
function addElementHTML(){

    var blockGauche = document.createElement('div');
    blockGauche.setAttribute('id','blockGauche');

    //Score
    var para = document.createElement('p');
    para.setAttribute('id','parascore');
    var score = document.createElement("span");
    score.setAttribute('id','points');
    var content = document.createTextNode(points);
    score.appendChild(content);
    var contentpara = document.createTextNode("Score : ");
    para.append(contentpara, score);
    var currentDiv = document.getElementById('div1');

    blockGauche.appendChild(para);
    document.body.insertBefore(blockGauche, currentDiv);

    //Vie
    var paravie = document.createElement('p');
    paravie.setAttribute('id','paravie');
    var vie = document.createElement("span");
    vie.setAttribute('id','vie');
    var contentvie = document.createTextNode(hp);
    vie.appendChild(contentvie);
    var contentparavie = document.createTextNode("Vie: ");
    paravie.append(contentparavie, vie);

    

    blockGauche.appendChild(paravie);
    document.body.insertBefore(blockGauche, currentDiv);

    //aide touches 
    var aide = document.createElement('div');
    aide.setAttribute('id','aide');
    var fleche = document.createElement('p');
    var clickGauche = document.createElement('p');
    var clickDroite = document.createElement('p');

    var contentFleche = document.createTextNode('Déplacement : fleches directionnelles');
    var contentClG = document.createTextNode('Missile : click gauche');
    var contentClD = document.createTextNode('Laser : click droit');

    fleche.appendChild(contentFleche);
    clickGauche.appendChild(contentClG);
    clickDroite.appendChild(contentClD);
    aide.append(fleche,clickGauche,clickDroite);

    blockGauche.appendChild(aide);
    document.body.insertBefore(blockGauche, currentDiv);

    var blockDroite = document.createElement('div');
    blockDroite.setAttribute('id','blockDroite');

    var ptest = document.createElement('p');
    ptest.setAttribute('id', 'test');

    var icon = document.createElement('i');
    icon.setAttribute('class', 'fas fa-heart');
    icon.setAttribute('id','first');
    var icon2 = document.createElement('i');
    icon2.setAttribute('class', 'fas fa-heart');
    icon2.setAttribute('id','second');
    var icon3 = document.createElement('i');
    icon3.setAttribute('class', 'fas fa-heart');
    icon3.setAttribute('id','third');

    ptest.append(icon,icon2,icon3);

    blockDroite.append(ptest);

    document.body.insertBefore(blockDroite, currentDiv);


    var recapGame = document.createElement('div');
    recapGame.setAttribute('id','recap');

    var title = document.createElement('p');
    title.setAttribute('id','title');
    title.appendChild(document.createTextNode('Game Over'));

    var scoreOver = document.createElement('p');
    scoreOver.setAttribute('id','score');
    scoreOver.appendChild(document.createTextNode('Score: '));

    var scorePoint = document.createElement('span');
    scorePoint.setAttribute('id','scorePoint');
    scorePoint.appendChild(document.createTextNode(points));

    scoreOver.appendChild(scorePoint);

    var retry = document.createElement('button');
    retry.setAttribute('id','retry');
    retry.setAttribute('onclick', 'restart()');
    //retry.appendChild(document.createTextNode("Recommencer"));

    var sp1 = document.createElement('span');
    var sp2 = document.createElement('span');
    var sp3 = document.createElement('span');
    var sp4 = document.createElement('span');

    sp1.setAttribute('class', 'sp');
    sp2.setAttribute('class', 'sp');
    sp3.setAttribute('class', 'sp');
    sp4.setAttribute('class', 'sp');


    recapGame.append(sp1,sp2,sp3,sp4);

    retry.appendChild(document.createTextNode("Recommencer"));

    recapGame.append(title,scoreOver,retry);
    document.body.insertBefore(recapGame, currentDiv);

    document.getElementById("recap").style.display = "none";

    /*var loader = document.createElement('div');
    loader.setAttribute('class', 'loader');
    document.body.insertBefore(loader, currentDiv);*/
}


function removeCoeur(){
    if(hp == 2)
        document.getElementById('first').className = "far fa-heart";
    else if(hp == 1)
        document.getElementById('second').className = "far fa-heart";
    else if(hp == 0)
        document.getElementById('third').className = "far fa-heart";
}

function retablirCoeur(){
    document.getElementById('first').className = "fas fa-heart";
    document.getElementById('second').className = "fas fa-heart";
    document.getElementById('third').className = "fas fa-heart";
}
//Verification fin de partie
function gameOver(){
    if(hp <= 0){
        document.getElementById("recap").style.display = "block";
        document.querySelector('#scorePoint').innerHTML = points;
        isOver = true;
        retablirDirection();
    }
}

//réinitialisation des directions à false
function retablirDirection(){
    direction = {
        avancer: false,
        reculer: false,
        droite: false,
        gauche: false
    }
}


//fonction preparant la nouvelle partie
//réinitialisation des variables
function restart(){
    clock.start();
    clockMissile.start();
    
    suppression();
    initParameter();

    document.querySelector("#points").innerHTML = points;
    document.querySelector("#vie").innerHTML = hp;
    document.getElementById("recap").style.display = "none";
    

    retablirCoeur();

    player.position.x = 0;
    player.position.y = -(window.innerHeight - 50) / 2;
}


//supprime tous les elements de la partie d'avant de la scene pour la nouvelle partie
function suppression(){
    //suppression missiles
    for(let i=0; i<missiles.length; i++){
        scene.remove(missiles[i]);
    }

    //suppression ennemies
    for(let i=0; i<ennemies.length; i++){
        scene.remove(ennemies[i]);
    }

    //suppression missilesEnnemies
    for(let i=0; i<missilesEnnemies.length; i++){
        scene.remove(missilesEnnemies[i]);   
    }

    //suppression morts
    for(let i=0; i<morts.length; i++){
        scene.remove(morts[i]);   
    }
}
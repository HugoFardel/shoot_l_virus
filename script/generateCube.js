//fonction générant un ennemie
function generateEnnemie(){
    var geometry = new THREE.BoxGeometry(30, 30, 0);
    var material = new THREE.MeshBasicMaterial({ map: typeEnnemies[Math.floor(Math.random()* Math.floor(typeEnnemies.length))], 
        transparent: true });
    cube = new THREE.Mesh(geometry, material);
    cube.position.y = 800;
    cube.position.x = Math.random() * (450 + 450) - 450;
    scene.add(cube);
    ennemies.push(cube);
}

//déplace les ennemies sur l'axe y (vertical)
function animateEnnemies(){
    for(let i=0; i<ennemies.length; i++){
        ennemies[i].position.y -= speedEnnemie;
        if(ennemies[i].position.y < -800){
            morts.push(ennemies[i]);
            ennemies.splice(i,1);
            i--;
        }
    }
}

//fonction verifiant la collision entre ennemie et joueur
function collision(){
    let distanceX=0, distanceY=0;
    for(let i=0; i<ennemies.length;i++){
        distanceX = Math.abs(ennemies[i].position.x - player.position.x);
        distanceY = Math.abs(ennemies[i].position.y - player.position.y);
        if(distanceX <= 40 && distanceY <= 40){ //40 car distance max(du centre) player 25 et 15 pour ennemies 
            morts.push(ennemies[i]);
            ennemies.splice(i,1);
            document.querySelector("#points").innerHTML = ++points;
            document.querySelector("#vie").innerHTML = --hp;
            removeCoeur();
            i--;
        }
    }
}

//fonction changeant le level en fonction du score
function changeLevel(){
    if(points == 20 || points == 40 || points == 60){
        level++;
        attributeLevel();
    }
}


//change les parametres de difficultés en fonction du niveau
function attributeLevel(){
    //vitesse apparition
    if(level==2){
        intervalApparition -= 0.3
        speedEnnemie += 0.1;
        intervalMissile -= 0.2;
    }
    else if(level==3){
        intervalApparition -= 0.3;
        speedEnnemie += 0.1;
        intervalMissile -= 0.2;
    }
    else if(level==4){
        intervalApparition -= 0.3;
        speedEnnemie += 0.1;
        intervalMissile -= 0.2;
    }
}
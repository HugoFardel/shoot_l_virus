//objet servant à indiquer la direction du déplacement
var direction = {
    avancer: false,
    reculer: false,
    droite: false,
    gauche: false
}


//détection appui touches et liaison avec le déplacement correspondant
function addevent(){
    addEventListener("keydown", function(event) {
       // event.preventDefault()
        if(!isOver){
            switch(event.keyCode){
                case 38:
                    direction.avancer = true;
                    break;
                case 37:
                    direction.gauche = true;
                    break;
                case 39:
                    direction.droite = true;
                    break;
                case 40:
                    direction.reculer = true;
                    break;
            }
        }
    });
    addEventListener("keyup", function(event) {
       // event.preventDefault()
        if(!isOver){
            switch(event.keyCode){
                case 38:
                    direction.avancer = false;
                    break;
                case 37:
                    direction.gauche = false;
                    break;
                case 39:
                    direction.droite = false;
                    break;
                case 40:
                    direction.reculer = false;
                    break;
            }
        }
    });

    addEventListener("mousedown", function(event){
        event.preventDefault();

        if(!isOver){
            //click droit
            if(event.button == 2 ){
                if(laser==null)
                    genereLaser();
            }
            else{
                genereMissile();
            }
        }
    });

    addEventListener("mouseup", function(event){
        event.preventDefault();
    });

    //desactive le menu du click droit
    addEventListener("contextmenu", function (event) {
        event.preventDefault();
    });
}



//mouvement cube en fonction des events
function moovePlayer(){
    if(direction.avancer){
        if(direction.gauche)
            avancerGauche();
        else if(direction.droite)
            avancerDroite();
        else
            avancer();
    }

    else if(direction.reculer){
        if(direction.gauche)
            reculerGauche();
        else if(direction.droite)
            reculerDroite();
         else
            reculer();
    }
    else if(direction.gauche){
        gauche();
    }
    else if(direction.droite){
        droite();
    }
}


function avancer(){
    let clone = player.clone();
    clone.position.y += speedPlayer;
    if(bordure(clone))
        player.position.y += speedPlayer;
}


function avancerGauche(){
    let clone = player.clone();
    clone.position.y += speedPlayer;
    clone.position.x -= speedPlayer;
    if(bordure(clone)){
        player.position.x -= speedPlayer;
        player.position.y += speedPlayer;
    }
}


function avancerDroite(){
    let clone = player.clone();
    clone.position.y += speedPlayer;
    clone.position.x += speedPlayer;
    if(bordure(clone)){
        player.position.x += speedPlayer;
        player.position.y += speedPlayer;
    }
}


function gauche(){
    let clone = player.clone();
    clone.position.x -= speedPlayer;
    if(bordure(clone)){
        player.position.x -= speedPlayer;
    }
}


function droite(){
    let clone = player.clone();
    clone.position.x += speedPlayer;
    if(bordure(clone)){
        player.position.x += speedPlayer;
    }
}


function reculer(){
    let clone = player.clone();
    clone.position.y -= speedPlayer;
    if(bordure(clone))
        player.position.y -= speedPlayer;
}


function reculerGauche(){
    let clone = player.clone();
    clone.position.y -= speedPlayer;
    clone.position.x -= speedPlayer;
    if(bordure(clone)){
        player.position.x -= speedPlayer;
        player.position.y -= speedPlayer;
    }
}


function reculerDroite(){
    let clone = player.clone();
    clone.position.y -= speedPlayer;
    clone.position.x += speedPlayer;
    if(bordure(clone)){
        player.position.x += speedPlayer;
        player.position.y -= speedPlayer;
    }
}


//Verifie la position pour éviter de sortie du cadre de l'écran
function bordure(obj){
    if(obj.position.x > -475 && obj.position.x < 475){
        if(obj.position.y > -(window.innerHeight - 50) / 2 - 4 && obj.position.y < 336){
            return true;
        }
    }
    return false;
}